#include "platform.h"
#include "nrf_drv_timer.h"


#ifdef ONEWIREHUB_FALLBACK_BASIC_FNs

extern const nrf_drv_timer_t TIMER_MICROS;

uint32_t micros() { return nrf_drv_timer_capture(&TIMER_MICROS, NRF_TIMER_CC_CHANNEL0); }; // original arduino-fn takes about 3 µs to process @ 16 MHz

void cli() { };
void sei() { };

void noInterrupts() { };

void interrupts() { };

#endif // ONEWIREHUB_FALLBACK_BASIC_FNs

