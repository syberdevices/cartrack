/*
 * versions.h
 *
 *  Created on: 3 окт. 2017 г.
 *      Author: oleynik
 */

#ifndef VERSIONS_H_
#define VERSIONS_H_

#define FIRMWARE_VERSION				"2.0.0"
#define HARDWARE_VERSION				"1.3.0"

#endif /* VERSIONS_H_ */
