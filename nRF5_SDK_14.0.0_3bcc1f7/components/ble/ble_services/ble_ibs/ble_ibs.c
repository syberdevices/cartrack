/*
 * ble_ibs.c
 *
 *  Created on: 14 сент. 2017 г.
 *      Author: oleynik
 */


#include "sdk_common.h"
#include "ble.h"
#include "ble_ibs.h"
#include "ble_srv_common.h"


#define BLE_UUID_IBS_IMMO_CHARACTERISTIC 	0x0003                      /**< The UUID of the IMMO Characteristic. */
#define BLE_UUID_IBS_RX_CHARACTERISTIC 	0x0002                      /**< The UUID of the RX Characteristic. */
#define BLE_UUID_IBS_ACC_CHARACTERISTIC 0x0004
#define BLE_UUID_IBS_DOORS_CHARACTERISTIC 0x0005
#define BLE_IBS_MAX_RX_CHAR_LEN        BLE_IBS_MAX_DATA_LEN        /**< Maximum length of the RX Characteristic (in bytes). */
#define BLE_IBS_MAX_IMMO_CHAR_LEN        BLE_IBS_MAX_DATA_LEN        /**< Maximum length of the IMMO Characteristic (in bytes). */

#define IBS_BASE_UUID                  {{0xF9, 0x5E, 0xCA, 0x0E, 0x99, 0x33, 0x11, 0xE7, 0xAB, 0xC4, 0xCE, 0xC2, 0x78, 0xB6, 0xB5, 0x0A}} /**< Used vendor specific UUID. */


/**@brief Function for handling the @ref BLE_GAP_EVT_CONNECTED event from the SoftDevice.
 *
 * @param[in] p_ibs     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_connect(ble_ibs_t * p_ibs, ble_evt_t const * p_ble_evt)
{
    p_ibs->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


/**@brief Function for handling the @ref BLE_GAP_EVT_DISCONNECTED event from the SoftDevice.
 *
 * @param[in] p_ibs     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_disconnect(ble_ibs_t * p_ibs, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_ibs->conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief Function for handling the @ref BLE_GATTS_EVT_WRITE event from the SoftDevice.
 *
 * @param[in] p_ibs     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_write(ble_ibs_t * p_ibs, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    ble_ibs_evt_t evt;
    evt.p_ibs = p_ibs;
    if (   ((p_evt_write->handle == p_ibs->immo_handles.cccd_handle)
    		|| (p_evt_write->handle == p_ibs->acc_handles.cccd_handle)
			|| (p_evt_write->handle == p_ibs->doors_handles.cccd_handle))
        && (p_evt_write->len == 2))
    {
        if (ble_srv_is_notification_enabled(p_evt_write->data))
        {
            p_ibs->is_notification_enabled = true;
            evt.type = BLE_IBS_EVT_COMM_STARTED;
        }
        else
        {
            p_ibs->is_notification_enabled = false;
            evt.type = BLE_IBS_EVT_COMM_STOPPED;
        }
        p_ibs->data_handler(&evt);
    }
    else if (   (p_evt_write->handle == p_ibs->rx_handles.value_handle)
             && (p_ibs->data_handler != NULL))
    {
        evt.params.rx_data.p_data = p_evt_write->data;
        evt.params.rx_data.length = p_evt_write->len;
        evt.type = BLE_IBS_EVT_RX_DATA;
        p_ibs->data_handler(&evt);
    }
    else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}


/**@brief Function for adding IMMO characteristic.
 *
 * @param[in] p_ibs       Nordic UART Service structure.
 * @param[in] p_ibs_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t immo_char_add(ble_ibs_t * p_ibs, ble_ibs_init_t const * p_ibs_init)
{
    /**@snippet [Adding proprietary characteristic to the SoftDevice] */
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.char_props.read = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_ibs->uuid_type;
    ble_uuid.uuid = BLE_UUID_IBS_IMMO_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_IBS_MAX_IMMO_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_ibs->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_ibs->immo_handles);
    /**@snippet [Adding proprietary characteristic to the SoftDevice] */
}

static uint32_t acc_char_add(ble_ibs_t * p_ibs, ble_ibs_init_t const * p_ibs_init)
{
    /**@snippet [Adding proprietary characteristic to the SoftDevice] */
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.char_props.read = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_ibs->uuid_type;
    ble_uuid.uuid = BLE_UUID_IBS_ACC_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_IBS_MAX_IMMO_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_ibs->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_ibs->acc_handles);
    /**@snippet [Adding proprietary characteristic to the SoftDevice] */
}

static uint32_t doors_char_add(ble_ibs_t * p_ibs, ble_ibs_init_t const * p_ibs_init)
{
    /**@snippet [Adding proprietary characteristic to the SoftDevice] */
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.char_props.read = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_ibs->uuid_type;
    ble_uuid.uuid = BLE_UUID_IBS_DOORS_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_IBS_MAX_IMMO_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_ibs->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_ibs->doors_handles);
    /**@snippet [Adding proprietary characteristic to the SoftDevice] */
}


/**@brief Function for adding RX characteristic.
 *
 * @param[in] p_ibs       Nordic UART Service structure.
 * @param[in] p_ibs_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rx_char_add(ble_ibs_t * p_ibs, const ble_ibs_init_t * p_ibs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.write         = 1;
    char_md.char_props.write_wo_resp = 1;
    char_md.p_char_user_desc         = NULL;
    char_md.p_char_pf                = NULL;
    char_md.p_user_desc_md           = NULL;
    char_md.p_cccd_md                = NULL;
    char_md.p_sccd_md                = NULL;

    ble_uuid.type = p_ibs->uuid_type;
    ble_uuid.uuid = BLE_UUID_IBS_RX_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 1;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_IBS_MAX_RX_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_ibs->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_ibs->rx_handles);
}


void ble_ibs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    if ((p_context == NULL) || (p_ble_evt == NULL))
    {
        return;
    }

    ble_ibs_t * p_ibs = (ble_ibs_t *)p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_ibs, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_ibs, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_ibs, p_ble_evt);
            break;

        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
        {
            //notify with empty data that some immo was completed.
            ble_ibs_evt_t evt = {
                    .type = BLE_IBS_EVT_IMMO_RDY,
                    .p_ibs = p_ibs
            };
            p_ibs->data_handler(&evt);
            break;
        }
        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_ibs_init(ble_ibs_t * p_ibs, ble_ibs_init_t const * p_ibs_init)
{
    uint32_t      err_code;
    ble_uuid_t    ble_uuid;
    ble_uuid128_t ibs_base_uuid = IBS_BASE_UUID;

    VERIFY_PARAM_NOT_NULL(p_ibs);
    VERIFY_PARAM_NOT_NULL(p_ibs_init);

    // Initialize the service structure.
    p_ibs->conn_handle             = BLE_CONN_HANDLE_INVALID;
    p_ibs->data_handler            = p_ibs_init->data_handler;
    p_ibs->is_notification_enabled = false;

    /**@snippet [Adding proprietary Service to the SoftDevice] */
    // Add a custom base UUID.
    err_code = sd_ble_uuid_vs_add(&ibs_base_uuid, &p_ibs->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_ibs->uuid_type;
    ble_uuid.uuid = BLE_UUID_IBS_SERVICE;

    // Add the service.
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_ibs->service_handle);
    /**@snippet [Adding proprietary Service to the SoftDevice] */
    VERIFY_SUCCESS(err_code);

    // Add the RX Characteristic.
    err_code = rx_char_add(p_ibs, p_ibs_init);
    VERIFY_SUCCESS(err_code);

    // Add the IMMO Characteristic.
    err_code = immo_char_add(p_ibs, p_ibs_init);
    VERIFY_SUCCESS(err_code);

    err_code = acc_char_add(p_ibs, p_ibs_init);
    VERIFY_SUCCESS(err_code);

    err_code = doors_char_add(p_ibs, p_ibs_init);
    VERIFY_SUCCESS(err_code);

    return NRF_SUCCESS;
}


uint32_t ble_ibs_string_send(uint16_t handle, ble_ibs_t * p_ibs, uint8_t * p_string, uint16_t * p_length)
{
    ble_gatts_hvx_params_t hvx_params;
    ble_gatts_value_t gatts_value;

    VERIFY_PARAM_NOT_NULL(p_ibs);

    if ((p_ibs->conn_handle == BLE_CONN_HANDLE_INVALID))// || (!p_ibs->is_notification_enabled))
    {
        return NRF_ERROR_INVALID_STATE;
    }

    if (*p_length > BLE_IBS_MAX_DATA_LEN)
    {
        return NRF_ERROR_INVALID_PARAM;
    }

    memset(&hvx_params, 0, sizeof(hvx_params));
    memset(&gatts_value, 0, sizeof(gatts_value));

    gatts_value.len = *p_length;
    gatts_value.offset = 0;
    gatts_value.p_value = p_string;

//    if (handle == 0) {
//    	hvx_params.handle = p_ibs->immo_handles.value_handle;
//    	sd_ble_gatts_value_set(p_ibs->conn_handle, p_ibs->immo_handles.value_handle, &gatts_value);
//    }
//    else if (handle == 1) {
//    	sd_ble_gatts_value_set(p_ibs->conn_handle, p_ibs->acc_handles.value_handle, &gatts_value);
//    	hvx_params.handle = p_ibs->acc_handles.value_handle;
//    }
//    else if (handle == 2) {
//    	sd_ble_gatts_value_set(p_ibs->conn_handle, p_ibs->doors_handles.value_handle, &gatts_value);
//    	hvx_params.handle = p_ibs->doors_handles.value_handle;
//    }

    hvx_params.handle = handle;
    sd_ble_gatts_value_set(p_ibs->conn_handle, handle, &gatts_value);

//    hvx_params.handle = p_ibs->immo_handles.value_handle;
    hvx_params.p_data = p_string;
    hvx_params.p_len  = p_length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

    sd_ble_gatts_hvx(p_ibs->conn_handle, &hvx_params);

    return NRF_SUCCESS;
}
