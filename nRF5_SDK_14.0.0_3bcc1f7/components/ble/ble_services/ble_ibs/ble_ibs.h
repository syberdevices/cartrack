/*
 * ble_ibs.h
 *
 *  Created on: 14 сент. 2017 г.
 *      Author: oleynik
 */


#ifndef BLE_IBS_H__
#define BLE_IBS_H__

#include <stdint.h>
#include <stdbool.h>
#include "sdk_config.h"
#include "ble.h"
#include "ble_srv_common.h"
#include "nrf_sdh_ble.h"

#ifdef __cplusplus
extern "C" {
#endif

/**@brief   Macro for defining a ble_ibs instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */

#define BLE_IBS_BLE_OBSERVER_PRIO 2
#define BLE_IBS_DEF(_name)                                                                          \
static ble_ibs_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_IBS_BLE_OBSERVER_PRIO,                                                     \
                     ble_ibs_on_ble_evt, &_name)

#define BLE_UUID_IBS_SERVICE 0x0001                      /**< The UUID of the Nordic UART Service. */

#define OPCODE_LENGTH 1
#define HANDLE_LENGTH 2

/**@brief   Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
#if defined(NRF_SDH_BLE_GATT_MAX_MTU_SIZE) && (NRF_SDH_BLE_GATT_MAX_MTU_SIZE != 0)
    #define BLE_IBS_MAX_DATA_LEN (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - OPCODE_LENGTH - HANDLE_LENGTH)
#else
    #define BLE_IBS_MAX_DATA_LEN (BLE_GATT_MTU_SIZE_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH)
    #warning NRF_SDH_BLE_GATT_MAX_MTU_SIZE is not defined.
#endif

/**@brief   Nordic UART Service event types. */
typedef enum
{
    BLE_IBS_EVT_RX_DATA,           /**< Data received. */
    BLE_IBS_EVT_IMMO_RDY,            /**< Service is ready to accept new data to be transmitted. */
    BLE_IBS_EVT_COMM_STARTED,      /**< Notification has been enabled. */
    BLE_IBS_EVT_COMM_STOPPED,      /**< Notification has been disabled. */
} ble_ibs_evt_type_t;



/* Forward declaration of the ble_ibs_t type. */
typedef struct ble_ibs_s ble_ibs_t;

/**@brief   Nordic UART Service @ref BLE_IBS_EVT_RX_DATA event data.
 *
 * @details This structure is passed to an event when @ref BLE_IBS_EVT_RX_DATA occurs.
 */
typedef struct
{
    uint8_t const * p_data;           /**< A pointer to the buffer with received data. */
    uint16_t        length;           /**< Length of received data. */
} ble_ibs_evt_rx_data_t;

/**@brief   Nordic UART Service event structure.
 *
 * @details This structure is passed to an event coming from service.
 */
typedef struct
{
    ble_ibs_evt_type_t type;           /**< Event type. */
    ble_ibs_t * p_ibs;                 /**< A pointer to the instance. */
    union
    {
        ble_ibs_evt_rx_data_t rx_data; /**< @ref BLE_IBS_EVT_RX_DATA event data. */
    } params;
} ble_ibs_evt_t;

/**@brief   Nordic UART Service event handler type. */
typedef void (*ble_ibs_data_handler_t) (ble_ibs_evt_t * p_evt);

/**@brief   Nordic UART Service initialization structure.
 *
 * @details This structure contains the initialization information for the service. The application
 * must fill this structure and pass it to the service using the @ref ble_ibs_init
 *          function.
 */
typedef struct
{
    ble_ibs_data_handler_t data_handler; /**< Event handler to be called for handling received data. */
} ble_ibs_init_t;

/**@brief   Nordic UART Service structure.
 *
 * @details This structure contains status information related to the service.
 */
struct ble_ibs_s
{
    uint8_t                  uuid_type;               /**< UUID type for Nordic UART Service Base UUID. */
    uint16_t                 service_handle;          /**< Handle of Nordic UART Service (as provided by the SoftDevice). */
    ble_gatts_char_handles_t immo_handles;              /**< Handles related to the IMMO characteristic (as provided by the SoftDevice). */
    ble_gatts_char_handles_t rx_handles;              /**< Handles related to the RX characteristic (as provided by the SoftDevice). */
    ble_gatts_char_handles_t acc_handles;
    ble_gatts_char_handles_t doors_handles;
    uint16_t                 conn_handle;             /**< Handle of the current connection (as provided by the SoftDevice). BLE_CONN_HANDLE_INVALID if not in a connection. */
    bool                     is_notification_enabled; /**< Variable to indicate if the peer has enabled notification of the RX characteristic.*/
    ble_ibs_data_handler_t   data_handler;            /**< Event handler to be called for handling received data. */
};


/**@brief   Function for initializing the Nordic UART Service.
 *
 * @param[out] p_ibs      Nordic UART Service structure. This structure must be supplied
 *                        by the application. It is initialized by this function and will
 *                        later be used to identify this particular service instance.
 * @param[in] p_ibs_init  Information needed to initialize the service.
 *
 * @retval NRF_SUCCESS If the service was successfully initialized. Otherwise, an error code is returned.
 * @retval NRF_ERROR_NULL If either of the pointers p_ibs or p_ibs_init is NULL.
 */
uint32_t ble_ibs_init(ble_ibs_t * p_ibs, ble_ibs_init_t const * p_ibs_init);


/**@brief   Function for handling the Nordic UART Service's BLE events.
 *
 * @details The Nordic UART Service expects the application to call this function each time an
 * event is received from the SoftDevice. This function processes the event if it
 * is relevant and calls the Nordic UART Service event handler of the
 * application if necessary.
 *
 * @param[in] p_ble_evt     Event received from the SoftDevice.
 * @param[in] p_context     Nordic UART Service structure.
 */
void ble_ibs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);


/**@brief   Function for sending a string to the peer.
 *
 * @details This function sends the input string as an RX characteristic notification to the
 *          peer.
 *
 * @param[in] p_ibs       Pointer to the Nordic UART Service structure.
 * @param[in] p_string    String to be sent.
 * @param[inout] p_length Pointer Length of the string. Amount of sent bytes.
 *
 * @retval NRF_SUCCESS If the string was sent successfully. Otherwise, an error code is returned.
 */
uint32_t ble_ibs_string_send(uint16_t handle, ble_ibs_t * p_ibs, uint8_t * p_string, uint16_t * p_length);


#ifdef __cplusplus
}
#endif

#endif // BLE_IBS_H__

/** @} */

