#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "app_timer.h"
#include "ble_ibs.h"
#include "ble_dis.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "OneWireHub.h"
#include "DS2401.h"

#include "nrf_drv_timer.h"
#include "app_error.h"

#include "nrf_drv_gpiote.h"

#include "versions.h"

#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_saadc.h"

#include "nrf_drv_wdt.h"

#define IMMO	0
#define ACC 	1
#define DOORS	2

#define FPU_EXCEPTION_MASK 0x0000009F

nrf_drv_wdt_channel_id m_channel_id;

const nrf_drv_timer_t TIMER_MICROS = NRF_DRV_TIMER_INSTANCE(2);
const nrf_drv_timer_t TIMER_LED = NRF_DRV_TIMER_INSTANCE(1);

extern "C" void __cxa_pure_virtual(void) {
	APP_ERROR_CHECK(-1);
}

#define FEEDBACK_PIN 					22
#define ONEWIRE_PIN						5
#define PIN_OUT							18
#define PIN_LED							17

#define TIME_WAIT						100
#define TIME_CONNECT					1 * 1000

auto hub     = OneWireHub(ONEWIRE_PIN); // do an bus-timing-calibration on first sensor-attachment
auto ds2401A = DS2401( DS2401::family_code, 0x3A, 0x0B, 0xB1, 0x10, 0x00, 0x00 );    // Work - Serial Number

bool isConnect = false;
bool transmitIbut = false;

#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

#define DEVICE_NAME                     "UrentCar"                               /**< Name of device. Will be included in the advertising data. */
#define IBS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

#define APP_BLE_OBSERVER_PRIO           1                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ADV_INTERVAL                160                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      0                                         /**< The advertising timeout (in units of seconds). */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */


BLE_IBS_DEF(m_ibs);                                                                 /**< BLE IBS service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */

static uint16_t   m_conn_handle          = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */
static uint16_t   m_ble_ibs_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static ble_uuid_t m_adv_uuids[]          =                                          /**< Universally unique service identifier. */
{
    {BLE_UUID_IBS_SERVICE, IBS_SERVICE_UUID_TYPE}
};

nrf_saadc_value_t acc_value = 0;
nrf_saadc_value_t doors_value = 0;
//uint32_t acc_value = 0;
//uint32_t doors_value = 0;

nrf_saadc_channel_config_t channel_1_config;
nrf_saadc_channel_config_t channel_2_config;

void read_voltage() {
	uint32_t err_code = 0;
	acc_value = 0;
	doors_value = 0;

	err_code = nrf_drv_saadc_sample_convert(1, &acc_value);

    err_code = nrf_drv_saadc_channel_uninit(1);
	APP_ERROR_CHECK(err_code);
	err_code = nrf_drv_saadc_channel_init(2, &channel_2_config);
	APP_ERROR_CHECK(err_code);

	err_code = nrf_drv_saadc_sample_convert(2, &doors_value);

    err_code = nrf_drv_saadc_channel_uninit(2);
	APP_ERROR_CHECK(err_code);
	err_code = nrf_drv_saadc_channel_init(1, &channel_1_config);
	APP_ERROR_CHECK(err_code);

	if (acc_value >= 0) {
		acc_value = (float)acc_value * 3.6f / 4096.0f * 5.7f * 10.0f;
	}
	else {
		acc_value = 0;
	}
	if (doors_value >= 0) {
		doors_value = (float)doors_value * 3.6f / 4096.0f * 6.0f * 10.0f;
	}
	else {
		doors_value = 0;
	}
}

bool default_value = true;

void send() {

	uint8_t mask = 0;
	uint8_t acc = 0;
	uint8_t doors = 0;

	uint16_t length = 1;

	uint32_t err_code = 0;

	if (default_value) {
		default_value = false;
		goto transmit;
	}

	read_voltage();

	acc = acc_value;
	doors = doors_value;

	if(nrf_gpio_pin_read(FEEDBACK_PIN)) {
		mask &= ~(1<<IMMO);
	}
	else {
		mask |= (1 << IMMO);
	}

	if (((float)acc/10) >= 2) {
		mask |= (1 << ACC);
	}
	else {
		mask &= ~(1<<ACC);
	}

	if (((float)doors/10) >= 2) {
		mask |= (1 << DOORS);
	}
	else {
		mask &= ~(1<<DOORS);
	}

	transmit:
    do
    {
        err_code = ble_ibs_string_send(m_ibs.immo_handles.value_handle, &m_ibs, &mask, &length);
        if ( (err_code != NRF_ERROR_INVALID_STATE) && (err_code != NRF_ERROR_BUSY) )
        {
            APP_ERROR_CHECK(err_code);
        }
    } while (err_code == NRF_ERROR_BUSY);

    do
    {
        err_code = ble_ibs_string_send(m_ibs.acc_handles.value_handle, &m_ibs, &acc, &length);
        if ( (err_code != NRF_ERROR_INVALID_STATE) && (err_code != NRF_ERROR_BUSY) )
        {
            APP_ERROR_CHECK(err_code);
        }
    } while (err_code == NRF_ERROR_BUSY);

    do
    {
        err_code = ble_ibs_string_send(m_ibs.doors_handles.value_handle, &m_ibs, &doors, &length);
        if ( (err_code != NRF_ERROR_INVALID_STATE) && (err_code != NRF_ERROR_BUSY) )
        {
            APP_ERROR_CHECK(err_code);
        }
    } while (err_code == NRF_ERROR_BUSY);
}


/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

bool isActive = false;

static void ibs_data_handler(ble_ibs_evt_t * p_evt)
{

    if (p_evt->type == BLE_IBS_EVT_RX_DATA)
    {
		if (p_evt->params.rx_data.length == 1 && p_evt->params.rx_data.p_data[0] == 1) {
			send();
			goto exit;
		}

        for (uint32_t i = 0; i < p_evt->params.rx_data.length; i++)
        {
        	ds2401A.ID[i+1] = p_evt->params.rx_data.p_data[i];
        }

//        if(!isActive) {
			ds2401A.ID[7] = ds2401A.crc8(ds2401A.ID, 7);
			hub.attach(ds2401A);
//			isActive = true;
//        }

        exit:
		return;
    }
}
/**@snippet [Handling the data received over BLE] */


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t       err_code;
    ble_ibs_init_t ibs_init;

    ble_dis_init_t dis_init;

    memset(&dis_init, 0, sizeof(dis_init));

    ble_srv_ascii_to_utf8(&dis_init.fw_rev_str, (char *)FIRMWARE_VERSION);
    ble_srv_ascii_to_utf8(&dis_init.hw_rev_str, (char *)HARDWARE_VERSION);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);

    memset(&ibs_init, 0, sizeof(ibs_init));

    ibs_init.data_handler = ibs_data_handler;

    err_code = ble_ibs_init(&m_ibs, &ibs_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_ADV_EVT_IDLE:
            sleep_mode_enter();
            break;
        default:
            break;
    }
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Uibsed.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
        	isConnect = true;
            NRF_LOG_INFO("Connected");
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;

        case BLE_GAP_EVT_DISCONNECTED:
        	isConnect = false;
        	hub.detach(ds2401A);
            NRF_LOG_INFO("Disconnected");
            err_code = bsp_indication_set(BSP_INDICATE_IDLE);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

#if defined(S132)
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys = {BLE_GAP_PHY_AUTO, BLE_GAP_PHY_AUTO};
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;
#endif

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

         case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST:
        {
            ble_gap_data_length_params_t dl_params;

            // Clearing the struct will effectivly set members to @ref BLE_GAP_DATA_LENGTH_AUTO
            memset(&dl_params, 0, sizeof(ble_gap_data_length_params_t));
            err_code = sd_ble_gap_data_length_update(p_ble_evt->evt.gap_evt.conn_handle, &dl_params, NULL);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_EVT_USER_MEM_REQUEST:
            err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        {
            ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                               &auth_reply);
                    APP_ERROR_CHECK(err_code);
                }
            }
        } break;

        default:

            break;
    }
}

static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        m_ble_ibs_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_ibs_max_data_len, m_ble_ibs_max_data_len);
    }
    NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
                  p_gatt->att_mtu_desired_central,
                  p_gatt->att_mtu_desired_periph);
}

void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, 64);
    APP_ERROR_CHECK(err_code);
}

void bsp_event_handler(bsp_event_t event)
{
    uint32_t err_code;
    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BSP_EVENT_WHITELIST_OFF:
            if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
            {
                err_code = ble_advertising_restart_without_whitelist(&m_advertising);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
            }
            break;

        default:
            break;
    }
}

static void advertising_init(void)
{
    uint32_t               err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type          = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance = false;
    init.advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE; //BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;

    init.srdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.srdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

void timer_micros_event_handler(nrf_timer_event_t event_type, void* p_context) {

}

void timer_led_event_handler(nrf_timer_event_t event_type, void* p_context) {
	static uint32_t count = 0;
    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:
        	if(transmitIbut) {
        		static uint32_t transmitCount = 0;
        		if (transmitCount <= 20) {
					if (count > 1) {
						if ((nrf_gpio_pin_out_read(PIN_LED) == 0)){
							nrf_gpio_pin_set(PIN_LED);
							count = 0;
						}
						else {
							nrf_gpio_pin_clear(PIN_LED);
							count = 0;
						}
					}
					transmitCount++;
        		}
        		else {
        			transmitIbut = false;
        			transmitCount = 0;
        		}
        	}
        	else if (isConnect) {
        		if (count > 7) {
        			nrf_gpio_pin_clear(PIN_LED);
        			count = 0;
        		}
        		else if ((nrf_gpio_pin_out_read(PIN_LED) == 0) && (count > 3)){
        			nrf_gpio_pin_set(PIN_LED);
        			count = 0;
        		}
        	}
        	else {
        		if (count > 47) {
        			nrf_gpio_pin_clear(PIN_LED);
        			count = 0;
        		}
        		else if ((nrf_gpio_pin_out_read(PIN_LED) == 0) && (count > 3)){
        			nrf_gpio_pin_set(PIN_LED);
        			count = 0;
        		}
        	}
            break;

        default:

            break;
    }
    count++;
}

//void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
//{
//    nrf_drv_gpiote_out_toggle(PIN_OUT);
//    hub.detach(ds2401A);
//    send();
//}

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{


}


void saadc_init(void)
{
    ret_code_t err_code;
    nrf_drv_saadc_config_t saadc_config;

    //Configure SAADC
    saadc_config.low_power_mode = true;                                                   //Enable low power mode.
    saadc_config.resolution = NRF_SAADC_RESOLUTION_12BIT;                                 //Set SAADC resolution to 12-bit. This will make the SAADC output values from 0 (when input voltage is 0V) to 2^12=2048 (when input voltage is 3.6V for channel gain setting of 1/6).
    saadc_config.interrupt_priority = APP_IRQ_PRIORITY_LOW;                               //Set SAADC interrupt to low priority.
    saadc_config.oversample = NRF_SAADC_OVERSAMPLE_DISABLED;

    //Initialize SAADC
    err_code = nrf_drv_saadc_init(&saadc_config, saadc_callback);                         //Initialize the SAADC with configuration and callback function. The application must then implement the saadc_callback function, which will be called when SAADC interrupt is triggered
    APP_ERROR_CHECK(err_code);

    //Configure SAADC channel
    channel_1_config.reference = NRF_SAADC_REFERENCE_INTERNAL;                              //Set internal reference of fixed 0.6 volts
    channel_1_config.gain = NRF_SAADC_GAIN1_6;                                              //Set input gain to 1/6. The maximum SAADC input voltage is then 0.6V/(1/6)=3.6V. The single ended input range is then 0V-3.6V
    channel_1_config.acq_time = (nrf_saadc_acqtime_t)SAADC_CH_CONFIG_TACQ_10us;                                     //Set acquisition time. Set low acquisition time to enable maximum sampling frequency of 200kHz. Set high acquisition time to allow maximum source resistance up to 800 kohm, see the SAADC electrical specification in the PS.
    channel_1_config.mode = NRF_SAADC_MODE_SINGLE_ENDED;                                    //Set SAADC as single ended. This means it will only have the positive pin as input, and the negative pin is shorted to ground (0V) internally.
    channel_1_config.pin_p = NRF_SAADC_INPUT_AIN4;                                          //Select the input pin for the channel. AIN0 pin maps to physical pin P0.02.
    channel_1_config.pin_n = NRF_SAADC_INPUT_DISABLED;                                      //Since the SAADC is single ended, the negative pin is disabled. The negative pin is shorted to ground internally.
    channel_1_config.resistor_p = NRF_SAADC_RESISTOR_DISABLED;                              //Disable pullup resistor on the input pin
    channel_1_config.resistor_n = NRF_SAADC_RESISTOR_DISABLED;                              //Disable pulldown resistor on the input pin


    //Configure SAADC channel
    channel_2_config.reference = NRF_SAADC_REFERENCE_INTERNAL;                              //Set internal reference of fixed 0.6 volts
    channel_2_config.gain = NRF_SAADC_GAIN1_6;                                              //Set input gain to 1/6. The maximum SAADC input voltage is then 0.6V/(1/6)=3.6V. The single ended input range is then 0V-3.6V
    channel_2_config.acq_time = (nrf_saadc_acqtime_t)SAADC_CH_CONFIG_TACQ_10us;                                     //Set acquisition time. Set low acquisition time to enable maximum sampling frequency of 200kHz. Set high acquisition time to allow maximum source resistance up to 800 kohm, see the SAADC electrical specification in the PS.
    channel_2_config.mode = NRF_SAADC_MODE_SINGLE_ENDED;                                    //Set SAADC as single ended. This means it will only have the positive pin as input, and the negative pin is shorted to ground (0V) internally.
    channel_2_config.pin_p = NRF_SAADC_INPUT_AIN1;                                          //Select the input pin for the channel. AIN0 pin maps to physical pin P0.02.
    channel_2_config.pin_n = NRF_SAADC_INPUT_DISABLED;                                      //Since the SAADC is single ended, the negative pin is disabled. The negative pin is shorted to ground internally.
    channel_2_config.resistor_p = NRF_SAADC_RESISTOR_DISABLED;                              //Disable pullup resistor on the input pin
    channel_2_config.resistor_n = NRF_SAADC_RESISTOR_DISABLED;                              //Disable pulldown resistor on the input pin



    //Initialize SAADC channel
    err_code = nrf_drv_saadc_channel_init(1, &channel_1_config);                            //Initialize SAADC channel 0 with the channel configuration
    APP_ERROR_CHECK(err_code);
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

void wdt_event_handler(void)
{

}

static void power_manage(void)
{
  __set_FPSCR(__get_FPSCR()  & ~(FPU_EXCEPTION_MASK));
    (void) __get_FPSCR();
    NVIC_ClearPendingIRQ(FPU_IRQn);

    ret_code_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

bool finishTransmit = false;

int main(void)
{
	nrf_gpio_cfg_output(PIN_LED);
	nrf_gpio_pin_set(PIN_LED);
	nrf_gpio_cfg_input(FEEDBACK_PIN, NRF_GPIO_PIN_NOPULL);

    uint32_t err_code;

    err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    log_init();

    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();

    err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);

	nrf_drv_timer_config_t timer_cfg = {                                                                               \
		    .frequency          = (nrf_timer_frequency_t)NRF_TIMER_FREQ_1MHz,\
		    .mode               = (nrf_timer_mode_t)NRF_TIMER_MODE_TIMER,          \
		    .bit_width          = (nrf_timer_bit_width_t)NRF_TIMER_BIT_WIDTH_32,\
		    .interrupt_priority = TIMER_DEFAULT_CONFIG_IRQ_PRIORITY,                    \
		    .p_context          = NULL                                                  \
		};
	err_code = NRF_SUCCESS;

	err_code = nrf_drv_timer_init(&TIMER_MICROS, &timer_cfg, timer_micros_event_handler);

	nrf_drv_timer_enable(&TIMER_MICROS);

	err_code = NRF_SUCCESS;

	err_code = nrf_drv_timer_init(&TIMER_LED, &timer_cfg, timer_led_event_handler);

    nrf_drv_timer_extended_compare(
         &TIMER_LED, NRF_TIMER_CC_CHANNEL0, nrf_drv_timer_ms_to_ticks(&TIMER_LED, TIME_WAIT), NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);

	nrf_drv_timer_enable(&TIMER_LED);

	err_code = nrf_drv_gpiote_init();
	APP_ERROR_CHECK(err_code);
	nrf_drv_gpiote_out_config_t out_config;
	out_config.init_state = NRF_GPIOTE_INITIAL_VALUE_LOW;
	out_config.task_pin = false;

	err_code = nrf_drv_gpiote_out_init(PIN_OUT, &out_config);
	APP_ERROR_CHECK(err_code);

//    nrf_drv_gpiote_in_config_t in_config;
//    in_config.is_watcher = false;
//    in_config.hi_accuracy = true;
//	in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
//    in_config.pull = NRF_GPIO_PIN_NOPULL;
//
//    err_code = nrf_drv_gpiote_in_init(FEEDBACK_PIN, &in_config, in_pin_handler);
//    APP_ERROR_CHECK(err_code);
//
//    nrf_drv_gpiote_in_event_enable(FEEDBACK_PIN, true);

	saadc_init();

    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
    nrf_drv_wdt_enable();

	send();

    for (;;)
    {
    	nrf_drv_wdt_channel_feed(m_channel_id);
    	if (isConnect) {
//    		if (isActive) {
    			hub.poll();
//    		}
    		if (finishTransmit) {
    			hub.detach(ds2401A);
    			finishTransmit = false;
    		}
    	}
    	else {
    		power_manage();
    	}
    }
}


/**
 * @}
 */
